﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class CSV
    {
        static public List<string[]> ImportCSV(string file)
        {
            List<string[]> values = new List<string[]>();
            if (File.Exists(@"..\..\csv\" + file + ".csv"))
            {
                using (StreamReader reader = new StreamReader(@"..\..\csv\" + file + ".csv"))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        values.Add(line.Split(';'));
                    }
                }
            }
            return values;
        }
        static public void SaveCSV(string data,string path)
        {
            ClearCSV(path);
            using (StreamWriter sw = new StreamWriter(@"..\..\csv\" + path + ".csv"))
            {

                    sw.Write(data);

            }

        }
        static public void ClearCSV(string path)
        {
            FileStream fileStream = File.Open(@"..\..\csv\" + path + ".csv", FileMode.Open);
            fileStream.SetLength(0);
            fileStream.Close();
        }
    }

}
