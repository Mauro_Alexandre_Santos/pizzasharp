﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Component
    {
        private string name;
        private float unitPrice;
        public Component(string name, float unitPrice)
        {
            this.name = name;
            this.unitPrice = unitPrice;
        }
        public string Name
        {
            get{return name;}
        }
        public float UnitPrice
        {
            get { return unitPrice; }
        }
    }
}
