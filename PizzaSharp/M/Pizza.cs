﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Pizza
    {
        private int num;
        private List<Component> listOfComponents = new List<Component>();
        private int pastaType;
        private float price;
        public Pizza(int num ,int pastaType, List<Component> listOfComponents)
        {
            this.num = num;
            this.pastaType = pastaType;
            this.listOfComponents = listOfComponents;
        }
        public List<Component> ListOfComponents
        {
            get
            {

                return listOfComponents;

            }

            set
            {

                listOfComponents = value;

            }
            
        }
        public int PastaType
        {
            get
            {

                return pastaType;

            }

            set
            {

                pastaType = value;

            }
        }
        public float Price
        {
            get
            {
                return price;
            }
            //!
            set
            {
                price = value;
            }
        }
        public int Num
        {
            get
            {
                return num;
            }
        }
    }
}
