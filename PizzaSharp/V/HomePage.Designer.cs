﻿namespace PizzaSharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstCommandes = new System.Windows.Forms.ListBox();
            this.btnNew = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstCommandes
            // 
            this.lstCommandes.FormattingEnabled = true;
            this.lstCommandes.Location = new System.Drawing.Point(50, 57);
            this.lstCommandes.Name = "lstCommandes";
            this.lstCommandes.Size = new System.Drawing.Size(124, 212);
            this.lstCommandes.TabIndex = 0;
            this.lstCommandes.SelectedIndexChanged += new System.EventHandler(this.LstCommandes_SelectedIndexChanged);
            this.lstCommandes.DoubleClick += new System.EventHandler(this.lstCommandes_DoubleClick);
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(50, 300);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(124, 30);
            this.btnNew.TabIndex = 1;
            this.btnNew.Text = "Nouvelle commande";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.BtnNew_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(226, 342);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.lstCommandes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Page d\'accueil";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstCommandes;
        private System.Windows.Forms.Button btnNew;
    }
}

