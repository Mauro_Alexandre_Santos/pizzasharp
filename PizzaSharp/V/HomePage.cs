﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;

namespace PizzaSharp
{
    public partial class Form1 : Form
    {
        List<Pizza> orders = new List<Pizza>();
        public Form1()
        {
            InitializeComponent();
            orders = CreatePizza();
            ShowPizza();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            string text = "";
            foreach (Pizza pizza in orders)
            {
                text = text + pizza.PastaType + ";";
                foreach(Model.Component component in pizza.ListOfComponents)
                {
                    text = text + component.Name + ";";
                }
                text = text + System.Environment.NewLine;
            }
            CSV.SaveCSV(text,"Pizza");
        }

        private void lstCommandes_DoubleClick(object sender, EventArgs e)
        {
            SecondPage m = new SecondPage(orders[lstCommandes.SelectedIndex]);
            DialogResult result = m.ShowDialog();
            if (result == DialogResult.Yes)
            {
                orders[m.CurrentPizza.Num] = m.CurrentPizza;
            }
        }
        private List<Pizza> CreatePizza()
        {
            int index = 0;
            List<Model.Pizza> pizzaList = new List<Model.Pizza>();
            foreach (string[] pizza in CSV.ImportCSV("Pizza"))
            {
                List<Model.Component> components = new List<Model.Component>();
                foreach (string parameter in pizza.Skip(1))
                {
                    float price = GetPrices(parameter);
                    components.Add(new Model.Component(parameter,price));
                }
                int pasta=Int32.Parse(pizza[0]);
                pizzaList.Add(new Pizza(index,pasta, components));
                index++;
            }
            return pizzaList;
        }
        public static float GetPrices(string component)
        {
            float output = -1;
            foreach(string[] comp in CSV.ImportCSV("components"))
            {
                if (comp[0] == component)
                {
                    output = float.Parse(comp[1]);
                }
            }
            return output;
        }
        private void ShowPizza()
        {
            lstCommandes.Items.Clear();
            int index = 0;
            foreach (Pizza pizza in orders)
            {
                lstCommandes.Items.Add("Commande " +index);
                index++;
            }
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            List<Model.Component> list = new List<Model.Component>();
            SecondPage m = new SecondPage(new Pizza(orders.Count, 0, list));
            DialogResult result = m.ShowDialog();
            if (result == DialogResult.Yes)
            {
                orders.Add(m.CurrentPizza);
            }
            ShowPizza();
        }

        private void LstCommandes_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
} 
