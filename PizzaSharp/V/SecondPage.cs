﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;

namespace PizzaSharp
{
    public partial class SecondPage : Form
    {
        private Pizza currentPizza;
        public SecondPage()
        {
            InitializeComponent();
        }
        public SecondPage(Pizza inputPizza)
        {
            InitializeComponent();
            currentPizza = inputPizza;
            LoadData();
        }
        private void SecondPage_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
        }
        private void LoadData()
        {
            checkedListBox1.Items.Clear();
            checkedListBox2.Items.Clear();
            OrderLbl.Text = ""+currentPizza.Num;
            comboBox1.SelectedIndex = currentPizza.PastaType;
            foreach (string[] listComponent in CSV.ImportCSV("components"))
            {
                Boolean flag=false;
                foreach(Model.Component pizzaComponent in currentPizza.ListOfComponents)
                {
                    if (listComponent[0] == pizzaComponent.Name)
                    {
                        flag = true;
                    }
                }
                if (flag)
                {
                    checkedListBox1.Items.Add(listComponent[0]);
                }
                else
                {
                    checkedListBox2.Items.Add(listComponent[0]);
                }
            }
            UpdatePizzaPrice();
            PriceLbl.Text = "" + currentPizza.Price;
        }
        private void UpdatePizzaPrice()
        {
            float ComponentsPrice = 0;
            foreach (string[] listComponent in CSV.ImportCSV("components"))
            {
                
                foreach (Model.Component pizzaComponent in currentPizza.ListOfComponents)
                {
                    if (listComponent[0] == pizzaComponent.Name)
                    {
                        ComponentsPrice = ComponentsPrice + float.Parse(listComponent[1]);
                    }
                }

            /**
            if (currentPizza.ListOfComponents.Contains(new Model.Component(listComponent[0], float.Parse(listComponent[1]))))
            {
                price = price + float.Parse(listComponent[1]);
            }
            **/
            }
            currentPizza.Price = ComponentsPrice + GetPastaPrice();
        }
        private float GetPastaPrice() {
            float pastaPrice=0;
            switch (currentPizza.PastaType)
            {
                case 0:
                    pastaPrice = 5f;
                    break;
                case 1:
                    pastaPrice = 5.25f;
                    break;
                case 2:
                    pastaPrice = 5.5f;
                    break;
                case 3:
                    pastaPrice = 6f;
                    break;
            }
            return pastaPrice;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public Pizza CurrentPizza
        {
            get
            {
                return currentPizza;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            foreach (string component in checkedListBox2.CheckedItems)
            {
                AddComponent(component);
            }
            LoadData();
        }
        private void AddComponent(string component)
        {
            foreach (string[] listComponent in CSV.ImportCSV("components"))
            {
                if (listComponent[0] == component)
                {
                    currentPizza.ListOfComponents.Add(new Model.Component(listComponent[0],float.Parse(listComponent[1])));
                }
            }   
        }
        private void button2_Click(object sender, EventArgs e)
        {
            foreach (string component in checkedListBox1.CheckedItems)
            {
                RemoveComponent(component);
            }
            LoadData();
        }
        private void RemoveComponent(string component)
        {
            int index = 0;
            List<int> indexList = new List<int>();
            foreach (Model.Component pizzaComponent in currentPizza.ListOfComponents)
            {
                if (component == pizzaComponent.Name)
                {
                    indexList.Add(index);
                }
                index++;
            }
            foreach (int item in indexList)
            {
                currentPizza.ListOfComponents.RemoveAt(item);
            }
            
        }

        private void checkedListBox3_Click(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_Click(object sender, EventArgs e)
        {
            currentPizza.PastaType = comboBox1.SelectedIndex;
            LoadData();
        }
    }
}
